# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

import logging
logger = logging.getLogger('disaggregation')

class Appliance(object):
    """
    This class represents a real appliance, or more generaly an event
    on the network. One appliance can have many states, like 'on', 'off',
    'sleep', or any custom made state.

    :param name: This is the name given to the appliance
    :param state: This is the first state recorded for the appliance. If
         no state is given, it will be set to 'on' by default.
    :param data: This is the first data recorded for appliance. It can
         be None if an appliance needs to be created manualy.
    :param data_id: This is the id related to the data. It should be unique.
    """
    def __init__(self, name, data_set, state="on", data=None, data_id=None):
        self.name = name
        self.current_state = state
        self.data = {}
        self.data_set = data_set
        if data is not None:
            self.add_data(data, data_id, state)

    def rename(self, name):
        """
        Change the name of the appliance.

        :param name: The new name
        """
        self.name = name

    def rename_state(self, old, new):
        """
        Change the name of a state in this appliance.

        :param old: The name to change
        :param new: the new name
        """
        self.data[new] = self.data[old]
        del self.data[old]
        if self.current_state == old:
            self.current_state = new

    def add_data(self, data, data_id, state):
        """
        Add new data used to further get the distance to this appliance.

        :param data: The data to be added
        :param data_id: A unique identifier amongst all the appliances
            linked to the new data
        :param state: The state the data is linked to
        """
        logger.debug(data_id + " added to " + self.name + ":" + state)
        if state not in self.data.keys():
            self.data[state] = {}
            logger.info("New state " + state + " added to appliance " + self.name)
        self.data[state][data_id] = True
        self.data_set[data_id] = data

    def remove_data(self, data_id): #TODO need to refactor this ?
        """
        Remove the data linked to an id

        :param data_id: The id of the data to be removed
        """
        removed = False
        for s in self.data.keys():
            if data_id in self.data[s].keys():
                del self.data[s][data_id]
                del self.data_set[data_id]
                removed = True
                break
        if not removed:
            logger.warning(data_id + " is not in " + self.name)
        else:
            logger.debug(data_id + " removed from " + self.name)

    def get_data(self, data_id): #TODO need to refactor this ?
        """
        Get the data linked to an id
        :param data_id: The id of the data to be returned
        """
        for s in self.data.keys():
            if data_id in self.data[s].keys():
                #return self.data[s][data_id]
                return self.data_set[data_id]
        logger.warning(data_id + " is not in " + self.name)
        return [0]

    def get_n_sample(self):
        """
        Get the total number of data registered amongst all the states of this
        appliance.
        """
        n = 0
        for state in self.data.keys():
            n += len(self.data[state])
        return n

    def get_distance(self, state, data):
        """
        Get the distance from a state of this appliance to a given data.

        :param state: The state of the appliance to be compared to the data
        :param data: The data to compare the state to
        """
        smalest_distance = float("inf")
        smalest_index = -1
        if len(self.data[state]) > 0:
            logger.debug("len of " + self.name + " : " + str(len(self.data[state])))
            for index,record in self.data[state].iteritems():
                T = sum([abs(x-y) for (x,y) in zip(self.data_set[index],data)])
                if T < smalest_distance:
                    smalest_distance = T
                    smalest_index = index
        return smalest_distance

    def merge(self, appliance):
        self.data.update(appliance.data) #TODO still works with states, aaaaaand single data_set ?

    def __str__(self):
        return self.name

    def get_data_sample(self, state, n):
        """
        Get the n'th data sample of a given state.

        :param state: The number of the state from which the data is taken
        :param n: The number of the record returned
        """
        if len(self.data) > 0:
            state = state%len(self.data)
            if len(self.data[self.data.keys()[state]]): #TODO else ?
                n = n%len(self.data[self.data.keys()[state]])
                return self.data_set[self.data[self.data.keys()[state]].keys()[n]]
        else:
            return [0]

