#!/usr/bin/env python3
# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

from os.path import basename, splitext
import pickle
import numpy
import hashlib
from time import gmtime, strftime
import logging
import re
logger = logging.getLogger('disaggregation')
from Appliance import Appliance

def sha1():
    """
    Get a unique string
    """
    return  hashlib.sha1(strftime("%Y-%m-%d %H:%M:%S", gmtime())).hexdigest()

class Appliances(object):
    """
    A set of Appliance, with handlers to search the set
    """

    def __init__(self):
        self.map = {}                  # the set of appliances (index:name of the appliance)
        self.record = {}               # the record to know where data comes from
        self.data_set = {}
        self.index = -1                # index used to name new appliances
        self.last_association = ""   #TODO maybe useless ?

    def empty_appliance(self, new_name=None):
        """
        Creates a new appliance without any data in it.

        :param new_name: The name given to the appliance. If omitted, the name item_#
            where # is an increasing number.
        """
        if new_name is None:
            self.index += 1
            name = "item_" + str(self.index)
        else:
            name = new_name
        if not name in self.map.keys():
            self.map[name] = Appliance(name, self.data_set)
        else:
            logger.warning(name + " is already in the appliances")

    def new_appliance(self, data):
        """
        Create a new appliance with data in it. The name and state are default : 'item_#'
        and 'on'

        :param data: The data linked to the appliance.
        """
        self.index += 1
        name = "item_" + str(self.index)
        self.last_association = name
        if not name in self.map.keys():
            h = sha1()
            self.record[h] = name
            self.map[name] = Appliance(name, self.data_set, data=data, data_id=h)
        else:
            logger.warning(name + " is already in the appliances")

    def add_data(self, appliance, state, data):
        """
        Add new data to an appliance

        :param appliance: The name of the appliance
        :param state: The state to which the data is to be added
        :param data: The data to add
        """
        h = sha1()
        self.record[h] = appliance
        self.map[appliance].add_data(data, h, state)

    def delete(self, item):
        """
        Delete an appliance from the set.

        :param item: The name of the appliance to delete
        """
        if item in self.map.keys():
            del self.map[item] #TODO remove from self.record AND self.data_set too
        else:
            logger.warning(item + " not in appliances")

    def switch(self, key, appliance, state): #TODO make switch last
        """
        Remove the data from an appliance/state and add it to an other appliance/state.

        :param key: The identifier of the data to switch
        :param appliance: The name of the appliance into which the data is to be added.
        :param state: The name of the state into which the data is to be added.
        """
        if appliance in self.map.keys() and key in self.record.keys():
            tmp = self.data_set[key] #remove first to not remove what you added if in same appliance
            self.map[self.record[key]].remove_data(key)
            if self.map[self.record[key]].get_n_sample() == 0:
                del self.map[self.record[key]]
                logger.debug(self.record[key] + " removed from appliances")
            self.map[appliance].add_data(tmp, key, state)
            self.record[key] = appliance
        else:
            logger.warning(appliance + " not in appliances or " + key + " not in records")


    def load(self, filename):
        """
        Load an appliance from memory and add it to the set.

        :param filename: The file name.
        """
        try:
            name = splitext(basename(filename))[0]
            if not name in self.map.keys():
                with open(filename,'rb') as input:
                    self.map[name] = pickle.load(input)
                    for data_id in self.map[name].data_linked.keys():
                        self.data_set[data_id] = self.map[name].data_linked[data_id]
                        self.record[data_id] = name
                    del self.map[name].data_linked
                    self.map[name].data_set = self.data_set
                    self.map[name].current_state = "UNK"
                    logger.info(name + " loaded")
            else:
                logger.warning(basename(filename) + " is already in the appliances")
        except:
            logger.warning("File does not exist, or it is not correctly formatted : " + filename)

    def save(self, item, filename=None):#TODO allow to use different filename
        """
        Save an appliance to memory.

        :param item: The appliance to save.
        :param filename: The file name to save the appliance into. Default is the appliance name.
        """
        if filename == None:
            filename = item + ".pkl"
        if item in self.map.keys():
            try:
                self.map[item].data_linked = {}
                for state in self.map[item].data.keys():
                    for data_id in self.map[item].data[state].keys():
                        self.map[item].data_linked[data_id] = self.data_set[data_id]
                with open(filename,'wb+') as output:
                    pickle.dump(self.map[item], output)
            except:
                logger.error("Could not open file " + filename)
        else:
            logger.warning(item + " not in appliances")


    def merge(self, parent, child):
        keys = self.map.keys()
        if parent in keys and child in keys:
            self.map[parent].merge(self.map[child])
            del self.map[child]
        else:
            logger.warning(parent + " or " + child + " not in appliances")

    def rename(self, old, new): # TODO rename into self.record too
        """
        Rename an appliance

        :param old: The current name of the appliance.
        :param new: The new name of the appliance.
        """
        if re.match("[a-zA-Z0-9_]+:[a-zA-Z_]+",old) and re.match("[a-zA-Z0-9_]+:[a-zA-Z_]+",new):
            if old.split(":")[0] == new.split(":")[0]:
                if old.split(":")[0] in self.map.keys():
                    self.map[old.split(":")[0]].rename_state(old.split(":")[1], new.split(":")[1])
                else:
                    logger.warning(old.split(":")[0] + " not in appliances")
            else:
                logger.warning("Appliance names do not match for state renaming")
            return

        if old in self.map.keys():
            self.map[new] = self.map[old]
            self.map[new].rename(new)
            del self.map[old]
        else:
            logger.warning(old + " not in appliances")

    def set_state(self, name, state):
        """
        Set the current state of an appliance.

        :param name: The name of the appliance.
        :param state: The new state of the appliance.
        """
        #TODO check it exists
        self.map[name].current_state = state

    def get_closest(self, data):
        """
        Get the closest appliance and state of a given data

        :param data: The data to which the distance is calculated.
        :return: A tuple (appliance,state,distance)
        """
        return self.knn(data,5)

    def knn(self, data, n):
        if len(self.data_set)==0:
            return ("","","")
        distance = []
        sample_example = self.data_set[self.data_set.keys()[0]]
        #std_deviation = [numpy.std([self.data_set[record][i] for record in self.data_set]) for i in range(len(sample_example))]
        std_deviation = [1]*len(sample_example)
        for key in self.map.keys():
            for state in self.map[key].data.keys():
                duplicate = 0
                if state == self.map[key].current_state:
                    continue
                for record in self.map[key].data[state]:
                    duplicate += 1
                    T = sum([abs(x-y)/d for (x,y,d) in zip(self.data_set[record],data,std_deviation)])
                    distance.append((T, key+":"+state))
                while duplicate <= n/2: # add more fairness for little known events
                    duplicate+=1
                    distance.append((T, key+":"+state))
        best_distance = sorted(distance, key=lambda tup:tup[0])[:n]
        vote = {}
        for i in best_distance:
            if i[1] in vote:
                vote[i[1]] += 1
            else:
                vote[i[1]] = 1
        logger.debug("vote : " + str(vote))
        if not vote:
            return ("","","")
        best_vote = sorted(vote.iteritems(), key=lambda kv : kv[1], reverse=True)[0][0]
        return (self.map[best_vote.split(":")[0]], best_vote.split(":")[1], best_distance[0][0]) #TODO return the true distance for the label

    def get_appliance(self,n):
        """
        Get the n'th appliance of the set

        :param n: The number of the appliance to be returned.
        :return: An Appliance
        """
        keys = self.map.keys()
        try:
            return self.map[sorted(keys)[n%len(keys)]]
        except:
            pass


