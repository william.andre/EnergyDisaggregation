# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

from Appliances import Appliances
import socket
import pickle
import logging
logger = logging.getLogger('disaggregation')

class Associator(object):
    """
    Contains a set of Appliance and a function linking new data to know
    and labeled data.
    """


    def __init__(self, port=None, ip="127.0.0.1"):
        try:
            with open('appliances.pkl','rb') as input:
                self.appliances = pickle.load(input)
        except:
            logger.warning("No file loaded")
            self.appliances = Appliances()
        self.index = 0
        self.current_pointer = 0

        self.port = port
        if port:
            try:
                self.ip = ip
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((self.ip,self.port))
            except Exception as e:
                logger.warning("Socket exception : " + str(e))

    def new_item(self,fft):
        self.appliances.new_appliance(fft)


    def associate(self,fft):
        (closest,state,distance) = self.appliances.get_closest(fft)
        logger.debug("-"*30)
        logger.debug("closest : " + str(closest) + ":" + state + ",   " + str(distance))
        if distance < 0.3:
            logger.info("Last detected item : " + str(closest) + ":" + state)
            if self.port:
                self.s.send(str(closest)+":"+state+"\n")
            self.appliances.add_data(closest.name,state,fft)
            self.appliances.map[closest.name].current_state = state
            self.appliances.last_association = closest.name
        else:
            self.new_item(fft)
            logger.info("New item : " + self.appliances.last_association)
        logger.debug("-"*30)

