# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals
conf = {}

from EventDetector import EventDetector
from Associator import Associator
from Prompt import Prompt
from MicrophoneRecorder import MicrophoneRecorder
from display import Display

import numpy as np
import time
import argparse
import logging
logger = logging.getLogger('disaggregation')


class Disaggregator(object):
    """
    This is the main class, combining the user interfaces and the disaggregating task.
    """
    def __init__(self, run_display = False, run_prompt = True, port = None, ip = "127.0.0.1"):
        self.prompt = Prompt(self)
        if run_prompt:
            self.prompt.start()

        self.event_detector = EventDetector()
        self.associator = Associator(port=port, ip=ip)

        self.rate = 12000
        self.chunksize = self.rate / 50#256*16*2
        self.mic = MicrophoneRecorder(rate=self.rate,chunksize=self.chunksize)
        self.mic.start()

        self.display = Display(self)
        if run_display:
            self.display.restart()

        self.running = False
        self.base_smooth_n = 0
        self.last_event_smooth_n = 0
        self.baseFreq = [0]*(self.chunksize*2+1)
        self.lastEvent = [0]*len(self.baseFreq)

    def start(self,display=False):
        """
        Take new data and put it through the algorithms.
        """
        self.running = True
        detected=False
        while self.running:
            mic_frames = self.mic.get_frames()
            if len(mic_frames) == 0:
                time.sleep(0.01)
                continue
            fft = [abs(np.fft.rfft(frame)/4000000) for frame in mic_frames]
            self.display.new_data(fft,mic_frames[-1])

            #detect events
            for i in range(len(fft)):
                if not detected and self.event_detector.detect(fft[i], self.baseFreq):
                    detected=True
                    old_base = self.baseFreq
                if detected:
                    self.baseFreq = fft[i]
                    self.base_smooth_n = 0
                    self.last_event_smooth_n += 1
                    n = self.last_event_smooth_n
                    if n < 20:
                        lastSeenEvent = [(x-y) for (x,y) in zip(old_base,self.baseFreq)]
                        self.lastEvent = [(n*y+x)/(n+1) for (x,y) in zip(lastSeenEvent, self.lastEvent)]
                    else:
                        detected=False
                        self.last_event_smooth_n = 0
                        #self.associator.associate(self.lastEvent)
                        self.associator.associate([i if i>2*(sum(self.lastEvent)/len(self.lastEvent)) else 0 for i in self.lastEvent]) # filter out the small values
                        self.display.new_base(self.baseFreq, self.lastEvent, True)
                elif self.base_smooth_n < 50:
                    self.base_smooth_n+=1
                    n = self.base_smooth_n
                    self.baseFreq = [(n*x+y)/(n+1) for (x,y) in zip(self.baseFreq, fft[i])]
                    self.display.new_base(self.baseFreq, self.lastEvent, False)

            if not self.prompt.isAlive():
                self.running = False
        self.display.stop()

    def __del__(self):
        self.display.stop()
        self.prompt.stop()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--display", dest="display", help="Run the display at start", action="store_true")
    parser.add_argument("-p","--port", dest="port", help="Port to send activity to", type=int, default=None)
    parser.add_argument("-i","--ip", dest="ip", help="IP to send activity to", type=str, default="127.0.0.1")
    args = parser.parse_args()

    f = open('/dev/null', 'w')
    logging.basicConfig(stream=f,format='%(asctime)s %(module)s:%(lineno)-3d %(levelname)s: %(message)s')
    logger.setLevel(logging.DEBUG)

    try:
        d = Disaggregator(run_display=args.display, port=args.port, ip=args.ip)
        d.start()
    except KeyboardInterrupt:
        del d
    except Exception as e:
        print(e)
        d.__del__()
        raise
