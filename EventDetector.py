# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

import logging
logger = logging.getLogger('disaggregation')

class EventDetector(object):
    """
    Given a stream of new data, detect if a event occured based on previous data.
    """
    def __init__(self):
        self.THRESHOLD = 500
        self.NORM_DEGREE = 10.0
        self.event_thresh = 10

        self.event_count = 0
        self.no_event_count = 0
        self.throttle = 0.0
        self.event_throttle = 0.0

    def detect(self, fft, base):
        """
        Compare the current fft to the elected base fft.

        :param fft: The current read signal
        :param base: The current base signal
        """
        T = sum([abs(x-y)**self.NORM_DEGREE for (x,y) in zip(base,fft)])**(1/self.NORM_DEGREE)
        t = self.THRESHOLD*(1-self.no_event_count*0.009999)
        self.throttle = min(T/t,1)
        if T > t:
            self.event_count +=1
            self.no_event_count = max(0,self.no_event_count*0.99)
        else:
            self.event_count -=3
            self.event_count = max(0,self.event_count)

        self.event_throttle = min(1,self.no_event_count/100)
        if self.event_count > self.event_thresh:
            self.event_count = 0
            self.no_event_count = 0
            logger.debug("Event detected : " + str(T))
            return True
        else:
            self.no_event_count = min(self.no_event_count+1,100)
            return False


