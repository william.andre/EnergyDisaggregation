#!/usr/bin/env python

# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals
import sys
import glob

from prompt_toolkit import prompt
from prompt_toolkit.contrib.regular_languages.compiler import compile
from prompt_toolkit.contrib.regular_languages.completion import GrammarCompleter
from prompt_toolkit.contrib.regular_languages.lexer import GrammarLexer
from prompt_toolkit.lexers import SimpleLexer
from prompt_toolkit.styles import Style
from prompt_toolkit.patch_stdout import patch_stdout, StdoutProxy
from prompt_toolkit.contrib.completers import PathCompleter, WordCompleter
from prompt_toolkit.completion import Completer, Completion

from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory

from threading import Thread, Event
import logging
logger = logging.getLogger('disaggregation')

#TODO add setbase
function = sorted(['rename','merge','delete','list','save','load','display','debug_level','switch','new_empty_appliance', 'set'])
debug_level = ['DEBUG','INFO','WARNING','ERROR']


def create_grammar():
    return compile("""
        (\s*  (?P<function>debug_level)   \s+   (?P<debug_level>{debug_level})                                                             \s*)   |
        (\s*  (?P<function>load)     \s+   (?P<path>[a-zA-Z\._\*\-]+)                                                                        \s*)   |
        (\s*  (?P<function>save)     \s+   (?P<appliance>[a-zA-Z0-9\-_]+)                                                                  \s*)   |
        (\s*  (?P<function>merge)    \s+   (?P<appliance>[a-zA-Z0-9\-_]+)   \s+   (?P<into>into)   \s+   (?P<appliance2>[a-zA-Z0-9\-_]+)   \s*)   |
        (\s*  (?P<function>display|list)                                                                                                   \s*)   |
        (\s*  (?P<function>delete)   \s+   (?P<appliance>[a-zA-Z0-9\-_]+)                                                                  \s*)   |
        (\s*  (?P<function>switch)   \s+   (?P<record>[a-f0-9]+)  \s+ (?P<to>to) \s+ (?P<appliance>[a-zA-Z0-9\-_]+:[a-zA-Z]+)              \s*)   |
        (\s*  (?P<function>new_empty_appliance)   (\s+   (?P<appliance>[a-zA-Z0-9\-_]+))?                                                  \s*)   |
        (\s*  (?P<function>set)      \s+   (?P<appliance>[a-zA-Z0-9\-_]+):(?P<state>[a-zA-Z]+)                                             \s*)   |
        (\s*  (?P<function>rename)   \s+   (?P<appliance>[a-zA-Z0-9\-_]+)   \s+  (?P<as>as)  \s+  (?P<new_appliance>[a-zA-Z1-9\-_]+)       \s*)   |
        (\s*  (?P<function>rename)   \s+   (?P<appliance>[a-zA-Z0-9\-_]+:[a-zA-Z]+)   \s+  (?P<as>as)  \s+  (?P<new_appliance>[a-zA-Z1-9\-_]+:[a-zA-Z]+) \s*)
    """.format(
        debug_level="|".join(debug_level)
        )
    )


example_style = Style.from_dict({
    'operator':       '#33aa33 bold',
    'number':         '#aa3333 bold',
    'appliance':      'italic',

    'trailing-input': 'bg:#662222 #ffffff',
    })

class ApplianceCompleter(Completer):
    def __init__(self,d):
        self.loading = False
        self.d = d

    # had to remove asserts in prompt_toolkit.completion WTF ?, worked before
    def get_completions(self, document, complete_event):
        self.loading = True
        word_before_cursor = document.get_word_before_cursor()
        for word in sorted(self.d.associator.appliances.map.keys()):
            if word.startswith(word_before_cursor):
                c = Completion(word, -len(word_before_cursor))
                yield c
        self.loading = False


class Sha1Completer(Completer):
    def __init__(self,d):
        self.loading = False
        self.d = d

    # had to remove asserts in prompt_toolkit.completion WTF ?, worked before
    def get_completions(self, document, complete_event):
        self.loading = True
        word_before_cursor = document.get_word_before_cursor()
        for word in sorted(self.d.associator.appliances.record.keys()):
            if word.startswith(word_before_cursor):
                c = Completion(word, -len(word_before_cursor))
                yield c
        self.loading = False

class Prompt(Thread):
    def __init__(self,disaggregator):
        super(Prompt,self).__init__()
        self._stop = Event()
        self.d = disaggregator
        handler = logging.StreamHandler(StdoutProxy())
        formatter = logging.Formatter('%(asctime)s %(module)13s:%(lineno)-3d %(levelname)7s: %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    def stop(self):
        self._stop.set()

    def run(self):
        g = create_grammar()

        lexer = GrammarLexer(g, lexers={
            'function': SimpleLexer('class:operator'),
            'debug_level': SimpleLexer('class:number'),
            'record': SimpleLexer('class:number'),
            'into': SimpleLexer('class:operator'),
            'to': SimpleLexer('class:operator'),
            'as': SimpleLexer('class:operator'),
            'appliance' : SimpleLexer('class:appliance'),
            'appliance2' : SimpleLexer('class:appliance'),
            'path': SimpleLexer('class:number')
            })

        completer = GrammarCompleter(g, {
            'function': WordCompleter(function),
            'debug_level': WordCompleter(debug_level),
            'into': WordCompleter(['into']),
            'to': WordCompleter(['to']),
            'as': WordCompleter(['as']),
            'path': PathCompleter(file_filter=lambda n:n.endswith(".pkl")),
            'record': Sha1Completer(self.d),
            'appliance': ApplianceCompleter(self.d),
            'appliance2': ApplianceCompleter(self.d)
            })

        history = InMemoryHistory()

        try:
            while not self._stop.isSet():
                # Read input and parse the result.
                with patch_stdout():
                    text = prompt('>>> ', refresh_interval=1, lexer=lexer, completer=completer, complete_in_thread=True,style=example_style,complete_while_typing=False, history=history, auto_suggest=AutoSuggestFromHistory())
                logger.debug("command : " + text)
                m = g.match(text)
                if m:
                    vars = m.variables()
                else:
                    logger.warning('Invalid command : ' + text)
                    continue

                logger.debug(vars)

                fun = vars.get('function')

                if fun == 'debug_level':
                    level = vars.get('debug_level')
                    if level == 'DEBUG':
                        logger.setLevel(logging.DEBUG)
                    elif level == 'INFO':
                        logger.setLevel(logging.INFO)
                    elif level == 'WARNING':
                        logger.setLevel(logging.WARNING)
                    elif level == 'ERROR':
                        logger.setLevel(logging.ERROR)
                    else:
                        logger.warning("Unrecognized debug level")
                elif fun == 'delete':
                    self.d.associator.appliances.delete(vars.get('appliance'))
                elif fun == 'display':
                    self.d.display.restart()
                elif fun == 'list':
                    for a in self.d.associator.appliances.map.values():
                        print(str(a) + ":" + a.current_state)
                elif fun == 'load':
                    for path in glob.glob(vars.get('path')):
                        self.d.associator.appliances.load(path)
                elif fun == 'merge':
                    self.d.associator.appliances.merge(vars.get('appliance2'),vars.get('appliance'))
                elif fun == 'rename':
                    self.d.associator.appliances.rename(vars.get('appliance'),vars.get('new_appliance'))
                elif fun == 'set':
                    self.d.associator.appliances.set_state(vars.get('appliance'),vars.get('state'))
                elif fun == 'save':
                    self.d.associator.appliances.save(vars.get('appliance'))
                elif fun == 'switch':
                    self.d.associator.appliances.switch(vars.get('record'),vars.get('appliance').split(":")[0],vars.get('appliance').split(":")[1])
                elif fun == 'new_empty_appliance':
                    name = None
                    if vars.get('appliance'):
                        name = vars.get('appliance')
                    self.d.associator.appliances.empty_appliance(name)
                else:
                    logger.warning('Not implemented yet')



        except EOFError:
            logger.debug('EOF : quitting')
        except KeyboardInterrupt:
            logger.debug('KeyboardInterrupt : quitting')
