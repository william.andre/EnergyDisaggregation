# EnergyDisaggregation

EnergyDisaggregation is a tool developed under GPLv3 aiming at disaggregating the signal received by a split core current transformer, like [this one](https://www.amazon.de/YHDC-SCT013-100A-50mA-Split-Core-Stromsensor/dp/B01EFS7QUE).
It is developed in python on linux. However, it should run without any problem on other operating systems.

## Installation

First, make sure you have the dependencies for pyaudio, more precisely the headers of pulseaudio.
```
sudo apt-get install pusleaudio
```
```
wget portaudio.com/archives/pa_stable_v190600_20161030.tgz
tar -zxvf pa_stable_v190600_20161030.tgz
```
You can also take a more recent version from http://www.portaudio.com/download.html

In the decompressed file, run the classic
```
./configure && make
sudo make install
```


You may also have to run `sudo ldconfig` in the same folder.

You also need to make sure that you have the dependencies for numpy.
```
sudo apt-get install libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev 
sudo apt-get install libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev
sudo apt-get install python3-dev python3-numpy
```

Then make sure pip and virtualenv are installed or your system.
```
sudo apt-get update
sudo apt-get install git python-setuptools python-dev build-essential

sudo easy_install pip 
sudo pip install --upgrade virtualenv 
```

You can then clone this project and create a virutal environment  with 
```
git clone https://gitlab.com/william.andre/EnergyDisaggregation.git
virtualenv EnergyDisaggregation
```
`cd` into the new folder then activate the virtualenv : `source bin/activate`

Finaly install the dependencies with `pip install -r requirements.txt`

There currently seems to be a bug with the use of python_prompt_toolkit. A quick fix is to use the flag `-O` with python.
## Use
Make sure the current transfomer is set as your main microphone-sound input when  running the tool.  
You can use the tool by running `python -O Disaggregate.py`  
You can also check the options by running `python -O Disaggregate.py --help`

## Known issue
You may have those error messages when starting the script for the first time
> Cannot connect to server socket err = No such file or directory

> Cannot connect to server request channel

> jack server is not running or cannot be started

> JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock

> JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock

Juste run these two lines to solve this :
```
pulseaudio --kill  
jack_control  start
```