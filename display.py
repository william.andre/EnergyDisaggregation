# This file is part of EnergyDisaggregation.
#
# EnergyDisaggregation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EnergyDisaggregation is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EnergyDisaggregation.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import time
from threading import Thread, Lock
from pygame.locals import *
import numpy as np
import logging
logger = logging.getLogger('disaggregation')

scale = 1.0
colors = [(1.0,0.8,0.5),(0.5,1.0,0.5),(0.5,0.5,1.0),(0.1,0.3,0.9),(0,1.0,1.0)]
black = (0,0,0)
red = (50,0,0)
green = (0,50,0)

bright_red = (255,0,0)
bright_green = (0,255,0)
scale_fft = 1.0
def scaleFft(value, zoom=False):
    m=1
    if zoom:
        m=2
    return abs(value)**(scale_fft/2)

def updateScaleFft(val):
    global scale_fft
    scale_fft += val
    scale_fft = min(2,max(0.01,scale_fft))


#TODO add event detector threshold representation
class Display(object):
    def __init__(self, d):
        self.running = False
        self.lock = Lock()
        self.d = d
        self.appliances = self.d.associator.appliances
        self.rate = self.d.rate
        self.chunksize = self.d.chunksize
        self.freq_vect = np.fft.rfftfreq(self.chunksize,1./self.rate)
        self.time_vect = np.arange(self.chunksize,dtype=np.float32)
        logger.debug("freq_vect : " + str(len(self.freq_vect)) + "(" + str(self.freq_vect[0]) + " - " + str(self.freq_vect[-1]) + ")")
        logger.debug(self.freq_vect)
        logger.debug("time_vect : " + str(len(self.time_vect)) + "(" + str(self.time_vect[0]) + " - " + str(self.time_vect[-1]) + ")")

        self.fft_frames = []
        self.wave = []
        self.fft_base = [0]
        self.last_event = [0]
        self.updated = False
        self.last_frame_time = time.time()
        self.n_sample = 0
        self.n_appliance = 0

        pygame.font.init()

        self.myfont = pygame.font.SysFont('garuda',11)

        self.spectrogram = pygame.Surface((int(800*scale),int(190*scale)))
        self.spectrogram_pos = (0,0)
        self.current_color = 0

        self.waveform = (pygame.Surface((int(800*scale),int(200*scale))),(int(0*scale),int(200*scale)))
        self.frequency = (pygame.Surface((int(129*scale),int(200*scale))),(int(0*scale),int(400*scale)))
        self.freqDiff = (pygame.Surface((int(129*scale),int(200*scale))),(int(150*scale),int(400*scale)))
        self.freqBase = (pygame.Surface((int(129*scale),int(200*scale))),(int(300*scale),int(400*scale)))
        self.freqEvent = (pygame.Surface((int(129*scale),int(200*scale))),(int(450*scale),int(400*scale)))
        self.appliance = (pygame.Surface((int(129*scale),int(200*scale))),(int(600*scale),int(400*scale)))
        self.throttle = (pygame.Surface((int(129*scale),int(10*scale))),(int(150*scale),int(420*scale)))

    def new_data(self,frames,wave):
        with self.lock:
            if self.running:
                self.fft_frames.append(frames)
                self.wave = wave
                self.updated = True

    def new_base(self,base,event,hard):
        with self.lock:
            if self.running:
                self.fft_base = base
                if hard:
                    self.last_event = event
                    self.current_color = (self.current_color+1) % len(colors)
                    self.updated = True
                    text = self.myfont.render(self.appliances.last_association, False, (255,0,0))
                    text = pygame.transform.rotate(text, 90)
                    self.spectrogram.blit(text,(int(780*scale),int(20*scale)))

    def displayUI(self):
        #scale_fft
        textSf = self.myfont.render("scale_fft=" +str(scale_fft), False, (255,0,0))
        self.window.blit(textSf,(0,15))
        #fps
        fps = 1/(time.time()-self.last_frame_time)
        self.last_frame_time = time.time()
        fpsSf = self.myfont.render("FPS=" + str(int(fps)), False, (255,0,0))
        self.window.blit(fpsSf,(0,0))

    def stop(self):
        with self.lock:
            self.running = False
        if hasattr(self, 'thread'):
            self.thread.join()
            logger.debug('Display closed')


    def restart(self):
        if not hasattr(self, 'thread') or not self.thread.is_alive():
            self.thread = Thread(target=self.run)
            logger.warning('display may be buggy if not started at the beginning, idk')
            self.thread.start()
        else:
            logger.warning('Display already running')

    def run(self):
        pygame.init()
        self.window = pygame.display.set_mode((int(800*scale),int(600*scale)))
        pygame.display.set_caption('Energy Disaggregator 3000')
        self.running = True
        self.start_time_key = time.time()

        # wait for first data
        b = False
        while not b:
            with self.lock:
                b = self.updated
            time.sleep(0.01)

        b = self.running
        while b:
            with self.lock:
                b = self.running
            # handle user input
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.running = False
                    pygame.display.quit()
                    logger.debug('Display closed')
                    return

            keys = pygame.key.get_pressed()
            if time.time()-self.start_time_key > 0.1:
                if keys[K_LEFT]:
                    updateScaleFft(-0.05)
                    self.start_time_key = time.time()
                if keys[K_RIGHT]:
                    updateScaleFft(+0.05)
                    self.start_time_key = time.time()
                if keys[K_g]:
                    self.n_appliance += 1
                    logger.debug("n_appliance : " + str(self.n_appliance))
                    self.start_time_key = time.time()
                if keys[K_h]:
                    self.n_appliance -= 1
                    logger.debug("n_appliance : " + str(self.n_appliance))
                    self.start_time_key = time.time()
                if keys[K_f]:
                    self.n_sample += 1
                    logger.debug("n_sample : " + str(self.n_sample))
                    self.start_time_key = time.time()
                if keys[K_j]:
                    self.n_sample -= 1
                    logger.debug("n_sample : " + str(self.n_sample))
                    self.start_time_key = time.time()

            # get data
            with self.lock:
                if self.updated:
                    if len(self.fft_frames)>0:
                        frames = self.fft_frames
                        wave = self.wave
                        self.updated = False
                        self.fft_frames = []
                        buffer_size = len(frames)

                        # slide spectrogram to left
                        temp_surf = self.spectrogram.copy()
                        self.spectrogram.blit(temp_surf,(-len(frames),0)) #TODO maybe use buffer_size

                        # add new data to spectrogram
                        spectr_sf = pygame.Surface((buffer_size,self.spectrogram.get_height()))
                        spectr_graph = pygame.PixelArray(spectr_sf)
                        for j in range(buffer_size):
                            fft = frames[j][0]
                            for i in range(len(fft)):
                                value = min(255,int(scaleFft(fft[i])*255))
                                spectr_graph[j][int((self.spectrogram.get_height()-1)*i/len(fft))] = tuple(value*x for x in colors[self.current_color])
                        del spectr_graph

                        # update waveform
                        self.drawWaveform(self.waveform, wave)

                        fft=frames[-1][0]
                        # update frequency
                        self.drawSpectrum(self.frequency, fft, "current frequency")

                        # see diff with base
                        self.drawSpectrum(self.freqDiff, [x-y for (x,y) in zip(self.fft_base, fft)], "diff")

                        # draw base
                        self.drawSpectrum(self.freqBase, self.fft_base, "current base")

                        # draw last event
                        self.drawSpectrum(self.freqEvent, self.last_event, "last event")

                        # draw throttle
                        self.drawThrottle(self.throttle, self.d.event_detector.throttle, self.d.event_detector.event_throttle)

            # draw appliance spectrum
            if(len(self.appliances.map)>=1):
                self.drawSpectrum(self.appliance, self.appliances.get_appliance(self.n_appliance).get_data_sample(0,self.n_sample), str(self.appliances.get_appliance(self.n_appliance))) #TODO add state

            self.spectrogram.blit(spectr_sf,(self.spectrogram.get_width()-buffer_size,0))
            self.window.blit(self.spectrogram,self.spectrogram_pos)
            self.displayUI()
            self.button("<",int(600*scale),int(425*scale),int(25*scale),int(25*scale),green,bright_green,action=self.appliance_m)
            self.button(">",int(700*scale),int(425*scale),int(25*scale),int(25*scale),green,bright_green,action=self.appliance_p)
            self.button("^",int(700*scale),int(450*scale),int(25*scale),int(25*scale),green,bright_green,action=self.appliance_s_m)
            self.button("v",int(700*scale),int(475*scale),int(25*scale),int(25*scale),green,bright_green,action=self.appliance_s_p)
            pygame.display.update()

            with self.lock:
                if not self.updated:
                    time.sleep(0.02)

        pygame.display.quit()
        logger.debug('Display closed')

    def appliance_p(self):
        if time.time()-self.start_time_key > 0.1:
            self.n_appliance += 1
            logger.debug("n_appliance : " + str(self.n_appliance))
            self.start_time_key = time.time()

    def appliance_m(self):
        if time.time()-self.start_time_key > 0.1:
            self.n_appliance -= 1
            logger.debug("n_appliance : " + str(self.n_appliance))
            self.start_time_key = time.time()

    def appliance_s_p(self):
        if time.time()-self.start_time_key > 0.1:
            self.n_sample -= 1
            logger.debug("n_sample : " + str(self.n_sample))
            self.start_time_key = time.time()

    def appliance_s_m(self):
        if time.time()-self.start_time_key > 0.1:
            self.n_sample -= 1
            logger.debug("n_sample : " + str(self.n_sample))
            self.start_time_key = time.time()

    def drawSpectrum(self,container, spectrum, text):
        #spectrum = spectrum[0:100]
        sf = pygame.Surface(container[0].get_size())
        graph = pygame.PixelArray(sf)
        sf.fill((0,0,0))
        for i in range(len(spectrum)):
            value = int(scaleFft(spectrum[i]/10)*(container[0].get_height()-1))
            graph[int((container[0].get_width()-1)*i/len(spectrum))][(container[0].get_height()-1)-value:(container[0].get_height()-1)] = (255,255,255)
        del graph
        container[0].blit(sf,(0,0))
        textSf = self.myfont.render(text, False, (255,0,0))
        container[0].blit(textSf,(0,0))
        self.window.blit(container[0],container[1])

    def drawWaveform(self,container, wave):
        sf = pygame.Surface(container[0].get_size())
        graph = pygame.PixelArray(sf)
        sf.fill((0,0,0))
        for i in range(len(wave)):
            graph[int((container[0].get_width()-1.0)*i/len(wave))][100] = (50,50,50)
            graph[int((container[0].get_width()-1.0)*i/len(wave))][(wave[i]/330)+100] = (255,255,255)
        del graph
        container[0].blit(sf,(0,0))
        self.window.blit(container[0],container[1])

    def drawThrottle(self,container,throttle,event_throttle):
        sf = pygame.Surface(container[0].get_size())
        graph = pygame.PixelArray(sf)
        sf.fill((0,0,0))
        for i in range(int((container[0].get_width()-1)*throttle)):
            graph[i][0:int(container[0].get_height()/2)] = (255,0,0)
        for i in range(int((container[0].get_width()-1)*event_throttle)):
            graph[i][int(container[0].get_height()/2):container[0].get_height()-1] = (0,255,0)
        graph[container[0].get_width()-1][0:(container[0].get_height()-1)] = (255,255,255)
        del graph
        container[0].blit(sf,(0,0))
        self.window.blit(container[0],container[1])

    def __del__(self):
        if self.tread.isAlive():
            self.thread.join()
        pygame.quit()


    #https://pythonprogramming.net/making-interactive-pygame-buttons/
    def button(self, msg,x,y,w,h,ic,ac,action=None):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        if x+w > mouse[0] > x and y+h > mouse[1] > y:
            pygame.draw.rect(self.window, ac,(x,y,w,h))

            if click[0] == 1 and action != None:
                action()
        else:
            pygame.draw.rect(self.window, ic,(x,y,w,h))

        smallText = pygame.font.SysFont("comicsansms",20)
        textSurf, textRect = text_objects(msg, smallText)
        textRect.center = ( (x+(w/2)), (y+(h/2)) )
        self.window.blit(textSurf, textRect)

def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()
